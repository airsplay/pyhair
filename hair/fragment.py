import numpy as np
from pystrand import *
import scipy.interpolate as itp

class StrandFragment:
	def __init__(self):
		pass
	
	def spline_3d(self, strand, new_num):
		"Return a strand with more data"
		xs = strand.get_X()
		ys = strand.get_Y()
		zs = strand.get_Z()
		
		#plt.plot(xs)
		#plt.show()

		old_num = xs.size 
		
		vVertex = np.zeros((new_num, 3), dtype = np.float32)
		
		ts = np.arange(old_num)
		new_ts = np.linspace(0, old_num - 1, new_num )
		#new_ts = new_ts[1:-1]
		new_xs = itp.spline(ts, xs, new_ts)
		new_ys = itp.spline(ts, ys, new_ts)
		new_zs = itp.spline(ts, zs, new_ts)
		#plt.plot(new_xs)
		#plt.show()

		vVertex[:, 0] = new_xs
		vVertex[:, 1] = new_ys
		vVertex[:, 2] = new_zs
		
		return Strand(vVertex)

_sf = StrandFragment()
spline_3d = _sf.spline_3d

