from pystrand import *
from projection import Projection
from hair_file import HairFile
from hair import Hair
import numpy as np
import time


if __name__ == "__main__":
	f = Hair("strands00001.data")
	f.show_3d()
