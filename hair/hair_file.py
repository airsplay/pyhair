import struct
from pystrand import Strands, Strand, Vertex
#from tqdm import tqdm
import numpy as np

class HairFile:
	""" 
	The basic class handle the loading and unload of the strands binary.
	The origin cpp code is listed in the folder.
	I suppose that it should use.
	"""
	def __init__(self, *args, **kwargs):
		self.set_file(*args, **kwargs)
	
	def set_file(self, file_name, is_binary = True):
		"""
		load file file_name
		:param file_name: The file to be opened
		:param is_binary = True: If the file is a binary file
		"""
		if (file_name != ""):
			self.file_name = file_name
			self.is_binary = is_binary

	def load_strands(self):
		"""
		load strands from a strands file
		:return : The class strands

		The format of this file is:
		#(Number of strands)
		#(Number of point in strand 1)
		point,point,point,...
		#(Number of point in strand 2)
		poinrt ,point ,point 
		"""
		if self.is_binary: # Open the file at the start of reading
			self.f = open(self.file_name, "rb")
		else:
			self.f = open(self.file_name, "r")

		strands = Strands()
		
		n_strands = self.read_int()	
		for i in xrange(n_strands):
			#print i
			strands.add_strand(self._load_strand())

		self.f.close() # Close the file at the end of reading

		return strands
	
	def _load_strand(self):
		"""
		load a strand from the strands file
		Don't try to call this method directly
		"""
		n_vertex = self.read_int()
		buf = self.read_3float_buffer(n_vertex) # Read the binaries of n_vetex * 3 * sizeof(float) from the file
		#for i in xrange(n_vertex):
			#x, y, z = self.read_3float()
			#strand.add_vertex(Vertex(x, y, z))
		return Strand(buf = buf)
		
	def read_int(self):
		"""
		read an integer from the underlying file
		"""
		s = self.f.read(4)
		return struct.unpack("i", s)[0]
	
	def read_float(self):
		"""
		read a float from the underlying file
		"""
		s = self.f.read(4) 
		return struct.unpack("f", s)[0]

	def read_3float(self):
		"""
		read three float from the underlying file
		"""
		s = self.f.read(12)
		return struct.unpack("3f", s)
	
	def read_3float_buffer(self, size = 1):
		return self.f.read(12 * size)
	
if __name__ == "__main__":	# Test of this file and a simple example
	from time import clock
	print "Test of Creating"
	print "Test of basic loading"
	#print hf.read_int()
	#print hf.read_int()
	#print hf.read_3float()
	print "Test of stands loading"
	
	start_time = clock()
	for x in tqdm(xrange(1000)):
		hf = HairFile("strands00001.data")
		strands = hf.load_strands()
	end_time = clock()
	print (end_time - start_time)
	#strands.show()
	#strands.show_3d()
	
	
