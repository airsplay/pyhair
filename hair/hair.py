from pystrand import *
from hair_file import *
class Hair:
	def __init__(self, file_name = "", strands = -1):
		if file_name != "":
			self.load(file_name)

		if strands != -1:
			self.strands_3d = strands
	
	def load(self, file_name):
		f = HairFile(file_name)
		self.strands_3d = f.load_strands()
	
	def show_strand_3d(self, index):
		self.strands_3d.show_3d(index)
	
	def get_strand(self, index):
		return self.strands_3d.get_strand(index)
	
	def size(self):
		return self.strands_3d.size()

	def show_3d(self, index = -1):
		plt.ion()
		if index == -1:
			self.strands_3d.show_3d()



