import numpy as np

class Projection:
	"""
	A projection to perform class 
	:member _mat: A float32 numpy matrix
	"""

	def __init__(self, file_name = None, proj_matrix = None):
		self._mat = proj_matrix
		if file_name != None:
			self.load(file_name)

	def perform(self, vertex):
		"""
		vertex is either an 3D point or an vector of 3D points
		"""
		if self._mat == None:
			raise "non-initialized class"
		#return self._mat[]

		# Write as this for efficiency
		# _mat = [Kernel, bias; 0, 1]
		# result = Kernel * vertex + elementwise_multiply(bias, vertex)
		vertex = np.mat(vertex).transpose()
		#print self._mat
		#print self._mat[:3, :3]
		a = (self._mat[:-1, :-1] * vertex + np.multiply(self._mat[:-1, -1], vertex)).transpose()
		b = np.divide(a[:, :-1], a[:, 2])
		return b
		

	
	def load(self, pmat_file_name):
		"""
		load an vertex from file
		:param pmat_file_name: The file to save the pmat matrix
		"""
		self._file_name = pmat_file_name
		self._mat = np.mat(np.fromstring(open(pmat_file_name).read(), dtype = np.float32, sep = " ").reshape(4,4))


if __name__ == "__main__":
	p = Projection()
	p.load("strands00001_0004.pmat")
	vertex_1 = [1, 2, 3]
	vertex_2 = np.array(vertex_1)
	vertex_3 = [4,5,6]
	vertex_4 = np.tile(vertex_2, (3, 1))
	for vertex in [vertex_1, vertex_2, vertex_3, vertex_4]:
		print p.perform(vertex)
	


