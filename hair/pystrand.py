import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
import random
import fragment
#import cv2 as cv
class Strands:
	"""
	A wrapper of python build-in array.
	Still using a python built-in list for maintain
	"""
	def __init__(self, size = 0):
		self.vStrand = [] # Vector of strand

	def add_strand(self, strand):
		"""
		add a strand to the strand list
		"""
		self.vStrand.append(strand)
	
	def get_strand(self, index):
		"""
		Get a strands from the strand list
		"""
		return self.vStrand[index]
	
	def show(self, *args, **kwargs):
		"""
		Call the show method of class Strand
		Any parameter will send to the strand's show method
		"""
		for strand in self.vStrand:
			strand.show(*args, **kwargs)
	
	def to_2d(self, proj, *args, **kwargs):
		strands_2d = Strands()
		for strand in self.vStrand:
			strands_2d.add_strand(Strand2D(strand, proj))
		return strands_2d
	
	def show_3d(self, strand_id = -1, *args, **kwargs):
		"""
		Call the show method of class Strand
		Any parameter will send to the strand's show method

		:param strand_id: The particular index of strand to be shown
		"""
		if strand_id != -1:
			self.vStrand[strand_id].show_3d(*args, **kwargs)
			return
		fig = plt.figure()
		ax = fig.add_subplot(1,1,1, projection = '3d')
	#	for i in xrange(10):
	#		self.vStrand[i*1000].plot_3d(ax)
	#		plt.draw()
	#		time.sleep(1)
		for strand in self.vStrand:
			strand.plot_3d(ax)
			plt.draw()
			time.sleep(0.1)
	
	def show_2d(self, strand_id, *args, **kwargs):
		if strand_id != -1:
			self.vStrand[strand_id].show(*args, **kwargs)
			return
	
	def size(self):
		"""
		The number of strand
		"""
		return len(self.vStrand)

	def __iter__(self):
		"""
		Usage:for strand in strands
		"""
		return self.vStrand.__iter__()


class Strand:
	"""
	Change to a numpy version
	"""
	def __init__(self, buf = None, constant = False):
		"""
		From an String buffer(Actually a large amount of memory) or from a numpy array
		PLEASE DO NOT CHANGE THE _vVertex. BECAUSE IT WILL CAUSE 
		:param buf: The buffer which hold the entire matrix
		:param constant: If constant is true, you declare that you will not change the content of this class
		"""
		if buf != None:
			self._vVertex = np.frombuffer(buf, np.float32).reshape(-1, 3)	
		else:
			self._vVertex = []
	
	def add_vertex(self, vertex):
		"""
		Unused when use buffer to create the inner struct of hair matrix
		"""
		self._vVertex.append(vertex)
	
	def get_random_fragment(self, fragment_length, method = "default", *args, **kwargs):
		"""
		Get a part of this hair strand.
		:param fragment_length: The length of fragment
		:return: A random fragment from this strand
		TODO: Add "method" parameter
		
		The default method will just return a random sequence from the strand.
		"""
		l = self.size()
		#print l
		if l < fragment_length:
			raise "NOT_LONG_ENOUGH"
			#return fragment.spline_3d(self, fragment_length)
			
		start_range = [0, l - fragment_length]
		start_point = random.randint(start_range[0], start_range[-1])
		return Strand(self._vVertex[start_point:start_point + fragment_length, :])
		


	def get_constant_array(self):
		"""
		return an NONCONSTANT version of this array. But you declare that you will not change it.
		"""
		return self._vVertex

	def get_array(self):
		"""
		It will create a new region in memory to hold this array. You could change it.
		"""
		return np.array(self._vVertex)
		

	def show(self):
		"""
		For test
		"""
		for vertex in self._vVertex:
			pass
	
	def shit(self):
		"""
		...
		"""
		pass
	
	def get_X(self):
		return self._vVertex[:, 0]
	
	def get_Y(self):
		return self._vVertex[:, 1]

	def get_Z(self):
		return self._vVertex[:, 2]
	
	def plot_3d(self, ax):
		"""
		plot a 3d graph on the 3d axis ax
		:param ax: The 3d axis to be ploted
		"""
		xs = [vertex[0] for vertex in self._vVertex]
		ys = [vertex[1] for vertex in self._vVertex]
		zs = [vertex[2] for vertex in self._vVertex]

		X = np.array(xs)
		Y = np.array(zs)
		Z = np.array(ys)
		max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0

		mean_x = X.mean()
		mean_y = Y.mean()
		mean_z = Z.mean()
		ax.set_xlim(mean_x - max_range, mean_x + max_range)
		ax.set_ylim(mean_y - max_range, mean_y + max_range)
		ax.set_zlim(mean_z - max_range, mean_z + max_range)
		
		plt.xlabel('x')
		plt.ylabel('y')
		#plt.zlabel('z')

		ax.set_xlim(-0.4, 0.4)
		ax.set_zlim(1.3, 1.8)
		ax.set_ylim(-0.4, 0.4)

		ax.plot(xs = X, ys = Y, zs = Z, zdir = 'z')
		
	def show_3d(self):
		"""
		Plot and show the 3d graph of a strand
		"""
		fig = plt.figure()
		ax = fig.add_subplot(1,1,1, projection = '3d')
		self.plot_3d(ax)
		plt.show()
	
	def draw_3d(self):
		pass
	
	def size(self):
		return self._vVertex.shape[0]
	
	def __len__(self):
		return self.size()
		

class Strand2D(Strand):
	"""
	The 2D version of Strand.
	Typically has an N * 2 Numpy array
	"""
	
	def __init__(self, strand = None, proj = None):
		"""
		The construction of the 2D strand
		:param strand: which 3D strand used to construct
		:param proj: The projection class from 3D to 2D
		"""
		if strand == None:
			return
		else:
			self.load(strand, proj)
	
	def load(self, strand, proj = None):
		"""
		Actually load the strand.
		"""
		self._vVertex = proj.perform(strand._vVertex) # Use the proj class to perform projection
			
	def show(self):
		"""
		Show the strand on
		The defualt choice is pyplot
		"""
		print self._vVertex[:, 0]
		print self._vVertex[:, 1]
		
		fig = plt.figure()
		ax = fig.add_subplot(1,1,1)

		X = np.array(self._vVertex[:, 0])
		Y = np.array(self._vVertex[:, 1])
		max_range = np.array([X.max()-X.min(), Y.max()-Y.min()]).max() / 2.0

		mean_x = X.mean()
		mean_y = Y.mean()
		ax.set_xlim(mean_x - max_range, mean_x + max_range)
		ax.set_ylim(mean_y - max_range, mean_y + max_range)

		ax.plot(X, Y)
		plt.show()
	
	

class Vertex:
	"""
	Not Used for efficiency
	"""
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z
	
	def __getitem__(self, index):
		if index == 0:
			return self.x
		if index == 1:
			return self.y
		if index == 2:
			return self.z
		
	
if __name__ == "__main__":
	strand = Strand() # It's the test of old version
	for i in xrange(100):
		strand.add_vertex(Vertex(i, i, i))
	strand.show_3d()

