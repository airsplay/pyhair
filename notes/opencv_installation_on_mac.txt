I installed the openCV 2.4.9 on mac.
It's a FUCKING difficult thing. If you use BREW to install OPENCV 2.4.7, it will link to the default libpython2.7.5 of the system instead of the anaconda python library
So I must download the code and install it by myself.

Here is a typically openCV cmake instruction I used.

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local \
	-D PYTHON_EXECUTABLE=/Users/tanhao/Workspace/anaconda/bin/python \
	-D PYTHON_PACKAGES_PATH=/Users/tanhao/Workspace/anaconda/lib/python2.7/site-packages \
	-D PYTHON_LIBRARY=/Users/tanhao/Workspace/anaconda/lib/libpython2.7.dylib \
	-D PYTHON_INCLUDE_DIR=/Users/tanhao/Workspace/anaconda/include/python2.7 \
	-D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON \
	-D BUILD_EXAMPLES=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules ..

After doing this, you must relink libpython2.7.dylib under /usr/lib to the correct dylib under anaconde/lib.

ln -s balabala

It will install the correct cv2.so under
/usr/local/lib/python2.7/site-packages,
either copy it to the anaconda/lib/python2.7/site-packages or add it to the PYTHONPATH
